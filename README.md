# Finance Portal

Simple console-line application which uses data from the norwegian website Finansportalen to give quick analysis and response about the user's current bank choices. The application analysis input data from the user and presents better alternatives to what the user currently uses.

## Create a Hotkey

Do you want to make a simple execution ```.bat``` file? Awesome!

1. Open notepad
2. Write the following (make sure to set your correct path):
```
dart C:\Users\<user>\Documents\Gitlab\finance-portal\bin\main.dart bsu
pause
```
3. Make sure that ```Save as...``` is set to: ```All files (*.*)```.
4. Set ```File name``` ending to be ```.bat```.

## Examples

### BSU Execution

![BSU Example](imgs/bsu.png)

### Property Loan

![Property Loan Example](imgs/propertyloan.png)
