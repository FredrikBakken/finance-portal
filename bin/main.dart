import 'dart:io';

import 'package:finance_portal/bsu.dart';
import 'package:finance_portal/propertyLoan.dart';

import 'settings.dart';

void main(List<String> inputs) {
  // BSU
  if (inputs.contains('bsu') || inputs.contains('BSU')) {
    bsu(myBSU);
  }

  // Property Loan
  if (inputs.contains('lån') || inputs.contains('loan')) {
    print('Property Loan:');
    print('1. Execute with current settings');
    print('2. Update settings\n');

    String selection = stdin.readLineSync();

    if (selection == '2') {
      print('\nAge? (default: ${myPropertyLoan['age']})');
      askQuestion('age', false);

      print('\nProperty value? (default: ${myPropertyLoan['property value']})');
      askQuestion('property value', false);

      print('\nDown-payment time (years)? (default: ${myPropertyLoan['payment time']})');
      askQuestion('payment time', false);

      print('\nMinimum necessary savings (yes or no)?');
      askQuestion('min', true);

      if (!myPropertyLoan['min']) {
        print('\nHow much savings do you have?');
        askQuestion('savings', false);
      }
    }

    propertyLoan(myPropertyLoan);
  }
}

void askQuestion(String field, bool min) {
  String selectionInput = stdin.readLineSync();
  if (min) {
    myPropertyLoan[field] = selectionInput == 'yes';
  } else if (selectionInput != myPropertyLoan[field] && selectionInput != '') {
    myPropertyLoan[field] = int.parse(selectionInput);
  }
}
