// BSU
Map myBSU = {
  'bank name': 'Valdres Sparebank',
  'interest': 3.60,
  'product id': 754773,   // Details from the xlsx
  'version id': 1232120,  // Details from the xlsx
};


// Property Loan
Map myPropertyLoan = {
  'property value': 3000000,
  'savings': 450000,
  'payment time': 30,
  'age': 25,
  'min': false,
};
