import 'dart:io';

import 'package:spreadsheet_decoder/spreadsheet_decoder.dart';

import 'bsuBanks.dart';
import 'download.dart';


void bsu(Map myBSU) async {
  String path = 'downloads/';
  String filename = 'bsu.xlsx';
  String url = "https://www.finansportalen.no/services/kalkulator/banksparing/export?kalkulatortype=banksparing&totalt_innestaende=150000&alderstilbudAr=25&nasjonalt=ja&regionalt=ja&visUtenProduktpakker=ja&neiforutsettermedlemskap=ja&kraft_sparekonto_med_spareavtale=ja&kraft_sparekonto_uten_spareavtale=ja&bsu=ja&sortcolumn=effectiveInterestRate%2C-bank.name%2C-name&sortdirection=desc";

  File bsuFile = File('$path$filename');

  bsuFile = await downloadData(path, filename, url);

  await listResults(bsuFile, myBSU);

  cleanup(path);
}


void listResults(File bsuFile, Map myBSU) {
  var bytes = bsuFile.readAsBytesSync();
  var decoder = new SpreadsheetDecoder.decodeBytes(bytes, update: false);
  for (var table in decoder.tables.keys) {
    print('Data from: $table');

    bool skipped = false;

    for (var row in decoder.tables[table].rows) {
      if (skipped) {
        if (row[15] == 'Landsdekkende') {
          row[15] = row[15] + '   ';
        }

        var currentBank = BSUBanks(row[1], row[2], row[3], row[4], row[7], row[8], row[12], row[14], row[15]);

        if (currentBank.productID == myBSU['product id'] && currentBank.versionID == myBSU['version id'] || currentBank.bankName.toLowerCase() == myBSU['bank name'].toLowerCase()) {
          print('\nYour current bank, ${currentBank.bankName}, updates!');
          if (currentBank.normalizedInterest != myBSU['interest'] || currentBank.effectiveInterest != myBSU['interest']) {
            print('It seems that the interest rate you have given to the applicatino (in main.dart) and the one found at ' + 
            'Finansportalen are not equal. Make sure to check that the interest rates are somewhat where you expect it to be.');
            print('The interest rate given to the application: ${myBSU['interest']}%');
            print('The interest rates found at Finansportalen: (nomiell) ${currentBank.normalizedInterest}% and ' + 
            '(effektiv) ${currentBank.effectiveInterest}%');
          } else {
            print('No updates/changes on the current rate: ${myBSU['interest'].toStringAsFixed(2)}%');
          }
          return;
        }

        print(
          '(nomiell) ${currentBank.normalizedInterest.toStringAsFixed(2)}%  ' +
          '(effektiv) ${currentBank.effectiveInterest.toStringAsFixed(2)}%  ' +
          '${currentBank.marketArea}  ' +
          '${currentBank.bankName}  ' +
          '${currentBank.bankURL}'
        );
      } else {
        print('\nBanks with better interest rates than your bank!');
        skipped = true;
      }
    }
  }
}
