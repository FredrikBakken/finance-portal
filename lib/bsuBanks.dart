class BSUBanks {
  double productID;
  double versionID;
  String name;
  String published;
  double effectiveInterest;
  double normalizedInterest;
  String bankURL;
  String bankName;
  String marketArea;

  BSUBanks(
    this.productID,
    this.versionID,
    this.name,
    this.published,
    this.effectiveInterest,
    this.normalizedInterest,
    this.bankURL,
    this.bankName,
    this.marketArea
  );
}
