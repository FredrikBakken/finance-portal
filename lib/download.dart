import 'dart:io';

import 'package:http/http.dart' as http;

Future downloadData(String path, String filename, String url) async {
  Future<File> bsuFile;

  await new Directory(path).create(recursive: true);

  await http.get(url).then((response) {
    bsuFile = new File('${path}${filename}').writeAsBytes(response.bodyBytes);
  });

  return bsuFile;
}


void cleanup(String path) {
  new Directory(path).delete(recursive: true);
}
