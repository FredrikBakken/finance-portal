import 'dart:io';

import 'package:spreadsheet_decoder/spreadsheet_decoder.dart';

import 'download.dart';
import 'propertyLoanBanks.dart';


void propertyLoan(Map myPropertyLoan) async {
  int loanAmount;
  int savings;
  if (myPropertyLoan['min']) {
    savings = (((myPropertyLoan['property value'] / 100) * 15).ceil());
  } else {
    savings = myPropertyLoan['savings'];
  }
  loanAmount = myPropertyLoan['property value'] - savings;

  String path = 'downloads/';
  String filename = 'propertyLoan.xlsx';
  String url = "https://www.finansportalen.no/services/kalkulator/boliglan/export?kalkulatortype=laan&laan_type=bolig&rentetakIgnore=ja&lanebelop=${loanAmount.toString()}&kjopesum=${myPropertyLoan['property value'].toString()}&lopetidtermin_value=${myPropertyLoan['payment time'].toString()}&alderstilbudAr=${myPropertyLoan['age'].toString()}&nasjonalt=ja&regionalt=ja&all=ja&neiforutsettermedlemskap=ja&rente=flytende_rente&standardlan=ja&rammelan=ja&forstehjemslan=ja&mellomfinansiering=ja&lan_fritidsbolig=ja&byggelan=ja&sortcolumn=demandsAnotherProduct%2CeffectiveInterestRate%2CmonthlyPayment%2Cbank.name%2Cname&sortdirection=asc";

  File propertyLoanFile = File('$path$filename');

  propertyLoanFile = await downloadData(path, filename, url);

  await listResults(propertyLoanFile, myPropertyLoan, loanAmount, savings);

  cleanup(path);
}

void listResults(File propertyLoanFile, Map myPropertyLoan, int loanAmount, int savings) {
  var bytes = propertyLoanFile.readAsBytesSync();
  var decoder = new SpreadsheetDecoder.decodeBytes(bytes, update: false);
  for (var table in decoder.tables.keys) {
    print('\n\nData from: $table');
    print('Total loan: ${loanAmount}');
    print('Savings: ${savings}');
    print('Savings percent: ${(savings/myPropertyLoan['property value'] * 100).toStringAsFixed(2)}%');

    int counter = 0;
    int numResults = 3;
    bool skipped = false;

    for (var row in decoder.tables[table].rows) {
      if (skipped) {
        if (row[19] == 'Landsdekkende') {
          row[19] = row[19] + '   ';
        }

        var currentBank = PropertyLoanBanks(row[1], row[2], row[3], row[4], row[7], row[8], row[9], row[16], row[18], row[19]);

        print(
          '(effektiv) ${currentBank.effectiveInterest.toStringAsFixed(2)}%  ' +
          '(pr. måned) ${currentBank.monthlyPayment}kr  ' +
          '${currentBank.marketArea}  ' +
          '${currentBank.bankName}  ' +
          '${currentBank.bankURL}'
        );

        counter++;
        if (counter == numResults) {
          return;
        }

      } else {
        print('\nBest property loan interest rates!');
        skipped = true;
      }
    }
  }
}
