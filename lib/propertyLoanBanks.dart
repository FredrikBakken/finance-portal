class PropertyLoanBanks {
  double productID;
  double versionID;
  String name;
  String published;
  double effectiveInterest;
  double monthlyPayment;
  double loanAmount;
  String bankURL;
  String bankName;
  String marketArea;

  PropertyLoanBanks(
    this.productID,
    this.versionID,
    this.name,
    this.published,
    this.effectiveInterest,
    this.monthlyPayment,
    this.loanAmount,
    this.bankURL,
    this.bankName,
    this.marketArea
  );
}
